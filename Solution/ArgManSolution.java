import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author David Giasson david.giasson@claurendeau.qc.ca
 */
public final class ArgManSolution implements ArgMan {

    private Argument[] arguments;

    public ArgManSolution(String schema) throws ArgManException {
        ArgumentFactory argFactory = new ArgumentFactory();
        arguments = argFactory.makeArgumentsFromSchema(schema);
    }

    public ArgManSolution(String schema, String[] args) throws ArgManException {
        this(schema);
        loadArgs(args);
    }

    @Override
    public void loadArgs(String[] args) throws ArgManException {
        resetArguments();
        parseArguments(Arrays.asList(args));
        checkForMissingArguments();
    }

    private void resetArguments() {
        for (Argument arg : arguments) {
            if (arg != null) {
                arg.reset();
            }
        }
    }

    private void parseArguments(List<String> args) throws ArgManException {
        Iterator<String> argsIterator = args.iterator();
        while (argsIterator.hasNext()) {
            Argument arg = getArgument(argsIterator.next());
            arg.tryLoadValue(argsIterator);
        }
    }

    private void checkForMissingArguments() throws ArgManException {
        for (Argument arg : arguments) {
            if (arg != null && !arg.isReady()) {
                throw new ArgManException("Missing mandatory argument: " + arg.getName());
            }
        }
    }
    
    private Argument getArgument(String rawArgName) throws ArgManException {
        if (!Argument.isArgumentName(rawArgName)) {
            throw new ArgManException("Expected argument name but found: " + rawArgName);
        }
        return getArgument(rawArgName.charAt(1));
    }

    private Argument getArgument(char argName) throws ArgManException {
        Argument arg = arguments[argName - Argument.firstAllowedParameterName];
        if (arg == null) {
            throw new ArgManException("Unrecognized argument name: " + argName);
        }
        return arg;
    }
    
    @Override
    public boolean getBoolean(char paramName) throws ArgManException {
        Argument arg = getArgument(paramName);
        return arg.getBoolean();
    }

    @Override
    public int getInt(char paramName) throws ArgManException {
        Argument arg = getArgument(paramName);
        return arg.getInt();
    }

    @Override
    public String getString(char paramName) throws ArgManException {
        Argument arg = getArgument(paramName);
        return arg.getString();
    }
}
