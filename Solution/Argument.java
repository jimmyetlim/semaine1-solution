import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class Argument {

    public static final char firstAllowedParameterName = 'A';
    public static final char lastAllowedParameterName = 'z';
    
    protected char name;
    private boolean optional, assigned;

    public static int maxNumberOfArguments () {
        return lastAllowedParameterName - firstAllowedParameterName + 1;
    }

    public static boolean isArgumentName (String arg) {
        return arg.length() == 2
                && arg.charAt(0) == '-'
                && isParameterName(arg.charAt(1));
    }

    public static boolean isParameterName (char paramName) {
        return paramName >= firstAllowedParameterName
                && paramName <= lastAllowedParameterName;
    }
    
    public Argument (char paramName, boolean isOptional) {
        this.name = paramName;
        this.optional = isOptional;
        this.assigned = false;
        this.reset();
    }

    public final void reset () {
        this.resetValue();
        this.assigned = false;
    }

    protected abstract void resetValue ();

    public final void tryLoadValue (Iterator<String> argsIterator) throws ArgManException {
        try {
            this.loadValue(argsIterator);
            this.assigned = true;
        }
        catch (NoSuchElementException e) {
            throw new ArgManException("Missing value for argument " + name);
        }
    }

    protected abstract void loadValue (Iterator<String> argsIterator) throws ArgManException;

    public char getName () {
        return name;
    }

    public boolean isReady () {
        return optional || assigned;
    }

    public int getInt () {
        throw new RuntimeException("Configuration error: No integer value for parameter " + name);
    }

    public String getString () {
        throw new RuntimeException("Configuration error: No string value for parameter " + name);
    }

    public boolean getBoolean () {
        throw new RuntimeException("Configuration error: No boolean value for parameter " + name);
    }
}


class BooleanArgument extends Argument {

    public static final boolean defaultValue = false;
    private boolean value;

    public BooleanArgument (char paramName) {
        super(paramName, true);
    }

    @Override
    protected void resetValue () {
        value = defaultValue;
    }

    @Override
    protected void loadValue (Iterator<String> argsIterator) {
        value = true;
    }

    @Override
    public boolean getBoolean () {
        return value;
    }
}


class IntArgument extends Argument {

    public static final int defaultValue = 0;
    private int value;

    public IntArgument (char paramName, boolean isOptional) {
        super(paramName, isOptional);
    }

    @Override
    protected void resetValue () {
        value = defaultValue;
    }

    @Override
    protected void loadValue (Iterator<String> argsIterator) throws ArgManException {
        String rawValue = argsIterator.next();
        if (isArgumentName(rawValue)) {
            throw new ArgManException("No value provided to argument " + name);
        }
        try {
            this.value = Integer.parseInt(rawValue);
        }
        catch (NumberFormatException e) {
            throw new ArgManException("Invalid integer value provided to argument " + name);
        }
    }

    @Override
    public int getInt () {
        if (!isReady())
            throw new RuntimeException("Configuration error: No value loaded for parameter " + name);
        return value;
    }
}


class StringArgument extends Argument {

    public static final String defaultValue = "";
    private String value;

    public StringArgument (char paramName, boolean isOptional) {
        super(paramName, isOptional);
    }

    @Override
    protected void resetValue () {
        value = defaultValue;
    }

    @Override
    protected void loadValue (Iterator<String> argsIterator) throws ArgManException {
        value = argsIterator.next();
        if (isArgumentName(value))
            throw new ArgManException("No value provided to argument " + name);
    }

    @Override
    public String getString () {
        if (!isReady())
            throw new RuntimeException("Configuration error: No value loaded for parameter " + name);
        return value;
    }
}
