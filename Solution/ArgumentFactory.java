
public class ArgumentFactory {

    public static final String schemaSeparator = ",";
    public static final char booleanMarker = '$';
    public static final char intMarker = '#';
    public static final char stringMarker = '*';
    public static final char optionalMarker = '?';
    
    private Argument[] arguments;
    
    public ArgumentFactory () {
        arguments = new Argument[Argument.maxNumberOfArguments()];
    }
    
    public Argument[] makeArgumentsFromSchema (String schema) throws ArgManException {
        if (!schema.equals("")) {
            String[] parameterCodes = schema.split(schemaSeparator);
            for (String parameterCode : parameterCodes)
                addArgument(makeArgumentFromCode(parameterCode));
        }
        return arguments;
    }

    private void addArgument (Argument arg) throws ArgManException {
        int argIndex = arg.getName() - Argument.firstAllowedParameterName;
        if (arguments[argIndex] == null)
            arguments[argIndex] = arg;
        else
            throw new ArgManException("Parameter name '" + arg.getName() + "' is already used");
    }

    public Argument makeArgumentFromCode (String parameterCode) throws ArgManException {
        char paramName = parameterCode.charAt(0);
        char paramType = parameterCode.charAt(1);
        
        if (parameterCode.length() == 2)
            return processParameter(paramName, paramType, false);
        else if (parameterCode.length() == 3 && parameterCode.charAt(2) == optionalMarker)
            return processParameter(paramName, paramType, true);
        else
            throw new ArgManException("Invalid parameter syntax '" + parameterCode + "' in schema");
    }

    private Argument processParameter (char paramName, char paramType, boolean isOptional)
    throws ArgManException {
        if (!Argument.isParameterName(paramName))
            throw new ArgManException("Invalid parameter name '" + paramName + "' in schema");

        switch (paramType) {
            case booleanMarker: // Booleans are always optional, so they never need the '?' marker
                return new BooleanArgument(paramName);
            case intMarker:
                return new IntArgument(paramName, isOptional);
            case stringMarker:
                return new StringArgument(paramName, isOptional);
            default:
                throw new ArgManException("Invalid parameter type '" + paramType + "' in schema");
        }
    }
}
